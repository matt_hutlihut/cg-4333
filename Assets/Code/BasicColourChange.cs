﻿using Photon.Pun;
using UnityEngine;

public class BasicColourChange : MonoBehaviour, IPunObservable
{
    public float changeTimerSeconds = 2f;
    private Color _color;

    private float timeIntoTimer = 0f;
    
    public void Start()
    {
        _color = Random.ColorHSV();
    }
    
    public void Update()
    {
        if (PhotonNetwork.LocalPlayer.IsMasterClient)
        {
            timeIntoTimer += Time.deltaTime;
            if (timeIntoTimer > changeTimerSeconds)
            {
                timeIntoTimer = 0f;
                _color = Random.ColorHSV();
            }
        }

        SetMaterialColour(_color);
    }

    private void SetMaterialColour(Color col)
    {
        GetComponent<Renderer>().material.color = col;
    }
    
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            Vector3 col = new Vector3(_color.r, _color.g, _color.b);
            stream.SendNext(col);
        }
        else
        {
            Vector3 col = (Vector3)stream.ReceiveNext();
            _color = new Color(col.x, col.y, col.z, 1f);
        }
    }
}