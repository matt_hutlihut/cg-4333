﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class RoomNameTextField : MonoBehaviour
{
    public static string ROOM_NAME_KEY = "RoomName";
    private InputField _inputField;
    
    void Start () 
    {
        string defaultRoomName = string.Empty;
        _inputField = GetComponent<InputField>();

        if (_inputField!=null)
        {
            if (PlayerPrefs.HasKey(ROOM_NAME_KEY))
            {
                defaultRoomName = PlayerPrefs.GetString(ROOM_NAME_KEY);
                _inputField.text = defaultRoomName;
            }
        }
    }
    
    public void SetRoomName()
    {
        string value = _inputField.text;
        
        if (string.IsNullOrEmpty(value))
        {
            Debug.LogError("Room Name is null or empty");
            return;
        }

        PlayerPrefs.SetString(ROOM_NAME_KEY, value);
    }
}
