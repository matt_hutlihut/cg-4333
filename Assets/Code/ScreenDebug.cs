using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

// Simple class to allow lines to be quickly displayed on screen for debugging purposes
public class ScreenDebug : MonoBehaviour
{
    private static readonly Dictionary<string, object> debugLines = new Dictionary<string, object>();

    private static ScreenDebug instance;
    private static GUIStyle guiStyle;

    private const string GO_NAME = "SCREEN_DEBUG_UTIL";
    private const int HEIGHT_SEPERATOR = 25;
    private const int WIDTH = 200;
    private const int TEXT_SIZE = 20;
    private const int MARGIN_OFFSET = 5;
    private readonly Color TEXT_COL = Color.green;

    private void Awake()
    {
        instance = FindObjectOfType<ScreenDebug>();
        guiStyle = new GUIStyle
        {
            normal = {textColor = TEXT_COL },
            fontSize = TEXT_SIZE
        };
    }
    
    #if !DEBUG
        [Conditional("DISABLE")]
    #endif
    public static void UpdateDebugLine(string key, object val)
    {
        EnsureObjectPresentInScene();
        debugLines[key] = val;
    }
        
    private void OnGUI()
    {
        EnsureObjectPresentInScene();
        
        Vector2 anchorPosition = new Vector2(TEXT_SIZE + MARGIN_OFFSET, TEXT_SIZE+ MARGIN_OFFSET);
        Vector2 size = new Vector2(WIDTH, TEXT_SIZE);
        
        int index = 0;
        foreach (KeyValuePair<string, object> debugLine in debugLines)
        {
            Vector2 pos = anchorPosition;
            pos.y += HEIGHT_SEPERATOR * index;
            Rect rect = new Rect(pos, size);
            
            GUI.Label(rect, $"{debugLine.Key} : {debugLine.Value}", guiStyle);
            
            index++;
        }
    }

    private static void EnsureObjectPresentInScene()
    {
        if(instance == null)
        {
            GameObject screenDebug = new GameObject(GO_NAME);
            instance = screenDebug.AddComponent<ScreenDebug>();
        }
    }
}