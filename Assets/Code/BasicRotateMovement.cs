﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class BasicRotateMovement : MonoBehaviour
{
    public float degChangePerSecond;
    public float radius;

    private Vector3 centerPos;
    private Vector3 localRadialPos;
    
    public void Start()
    {
        centerPos = transform.position;
        localRadialPos = Vector3.forward * radius;
    }
    
    public void Update()
    {
        if (GetComponent<PhotonView>().IsMine)
        {
            Quaternion extraRot = Quaternion.AngleAxis(degChangePerSecond * Time.deltaTime, Vector3.up);
            localRadialPos = extraRot * localRadialPos;
            transform.position = centerPos + localRadialPos;
        }
    }
}
