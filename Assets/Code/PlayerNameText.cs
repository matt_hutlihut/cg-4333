﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class PlayerNameText : MonoBehaviour, IPunObservable
{
    private TextMesh _text;
    void Awake()
    {
        _text = GetComponent<TextMesh>();
        _text.text = PhotonNetwork.LocalPlayer.NickName;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(_text.text);
        }
        else
        {
            _text.text = (string)stream.ReceiveNext();
        }
    }
}
