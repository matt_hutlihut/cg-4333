﻿using System.Linq;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Pun.Demo.PunBasics;
using UnityEngine;
using UnityEngine.UI;

using Photon.Realtime;
using UnityEngine.SceneManagement;

/// <summary>
/// Launch manager. Connect, join a random room or create one if none or all full.
/// </summary>
public class GameLauncher : MonoBehaviourPunCallbacks
{

	[Tooltip("The Ui Panel to let the user enter name, connect and play")]
	[SerializeField]
	private GameObject controlPanel;

	[Tooltip("The Ui Text to inform the user about the connection progress")]
	[SerializeField]
	private Text feedbackText;

	[Tooltip("The maximum number of players per room")]
	[SerializeField]
	private byte MAX_PLAYER_COUNT = 4;

	[Tooltip("The UI Loader Anime")]
	[SerializeField]
	private LoaderAnime loaderAnime;

	private TypedLobby _typedLobby;

	bool isConnecting;

	string gameVersion = "1";

	void Awake()
	{
		if (loaderAnime==null)
		{
			Debug.LogError("<Color=Red><b>Missing</b></Color> loaderAnime Reference.",this);
		}
		PhotonNetwork.AutomaticallySyncScene = false;
	}

	void Update()
	{
		ScreenDebug.UpdateDebugLine("NetworkClientState:", PhotonNetwork.NetworkClientState);
		ScreenDebug.UpdateDebugLine("Room:", PhotonNetwork.NetworkClientState == ClientState.Joined ? PhotonNetwork.CurrentRoom.Name : "Not In Room");
	}


	/// <summary>
	/// Start the connection process. 
	/// - If already connected, we attempt joining a random room
	/// - if not yet connected, Connect this application instance to Photon Cloud Network
	/// </summary>
	public void Connect()
	{
		// we want to make sure the log is clear everytime we connect, we might have several failed attempted if connection failed.
		feedbackText.text = "";

		// keep track of the will to join a room, because when we come back from the game we will get a callback that we are connected, so we need to know what to do then
		isConnecting = true;

		// hide the Play button for visual consistency
		controlPanel.SetActive(false);

		// start the loader animation for visual effect.
		if (loaderAnime!=null)
		{
			loaderAnime.StartLoaderAnimation();
		}

		if (PhotonNetwork.IsConnectedAndReady)
		{
			OnConnectedToMaster();
		}
		else
		{
			LogFeedback("Connecting...");
			PhotonNetwork.ConnectUsingSettings();
			PhotonNetwork.GameVersion = this.gameVersion;
		}
	}

	/// <summary>
	/// Logs the feedback in the UI view for the player, as opposed to inside the Unity Editor for the developer.
	/// </summary>
	/// <param name="message">Message.</param>
	void LogFeedback(string message)
	{
		// we do not assume there is a feedbackText defined.
		if (feedbackText == null) {
			return;
		}

		// add new messages as a new line and at the bottom of the log.
		feedbackText.text += System.Environment.NewLine+message;
	}

    public override void OnConnectedToMaster()
	{
		if (isConnecting)
		{
			LogFeedback("OnConnectedToMaster: Next -> Joining Lobby");

			_typedLobby = new TypedLobby("CG-4333_TestLobby", LobbyType.SqlLobby);
			bool joinLobby = PhotonNetwork.JoinLobby(_typedLobby);
			LogFeedback($"OnConnectedToMaster: Joining Lobby Sent {joinLobby}");
		}
	}

    public override void OnJoinedLobby()
    {
	    base.OnJoinedLobby();
	    string roomToJoin = PlayerPrefs.GetString(RoomNameTextField.ROOM_NAME_KEY);

	    LogFeedback($"OnJoinedLobby: Next -> Joining Room {roomToJoin}");
			
	    // Reflect CS options
	    string[] exposedProperties = Enumerable.Range(0, MAX_PLAYER_COUNT * 2).Select(i => $"C{i}").ToArray();
	    RoomOptions options = new RoomOptions 
	    { MaxPlayers = MAX_PLAYER_COUNT, 
		    PlayerTtl = 45000, 
		    CustomRoomProperties = new Hashtable
		    {
			    { nameof(PhotonNetwork.GameVersion), PhotonNetwork.GameVersion }
		    }, 
		    CustomRoomPropertiesForLobby = exposedProperties, 
		    PublishUserId = true 
	    };
			
	    PhotonNetwork.JoinOrCreateRoom(roomToJoin, options, _typedLobby);
    }

	public override void OnDisconnected(DisconnectCause cause)
	{
		LogFeedback("<Color=Red>OnDisconnected</Color> "+cause);
		Debug.Log("PUN Basics Tutorial/Launcher:Disconnected");
	}

	public void OnStartButtonClicked()
	{
		if (PhotonNetwork.LocalPlayer.IsMasterClient)
		{
			SceneManager.LoadScene("GameScene");
			photonView.RPC(nameof(LoadScene), RpcTarget.OthersBuffered);
		}
	}
	
	[PunRPC]
	public void LoadScene()
	{
		PhotonNetwork.IsMessageQueueRunning = false;
		SceneManager.LoadScene("GameScene");
	}
}
