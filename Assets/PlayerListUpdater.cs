﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

public class PlayerListUpdater : MonoBehaviourPunCallbacks
{
    public Font font;
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        CreateText(newPlayer.NickName, newPlayer.IsMasterClient);
    }

    public override void OnJoinedRoom()
    {
        foreach (Player player in PhotonNetwork.CurrentRoom.Players.Values)
        {
            CreateText(player.NickName, player.IsMasterClient);
        }
    }

    public override void OnLeftRoom()
    {
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Text[] texts = GetComponentsInChildren<Text>();

        foreach (var text in texts)
        {
            if (text.text == otherPlayer.NickName)
            {
                Destroy(text.gameObject);
            }
        }
    }

    private void CreateText( string playerName, bool masterclient)
    {
        GameObject UItextGO = new GameObject("Text2");
        UItextGO.transform.SetParent(transform);
        UItextGO.AddComponent<RectTransform>();

        Text text = UItextGO.AddComponent<Text>();
        text.text = playerName;
        text.font = font;
        text.fontSize = 15;
        text.color = masterclient ? Color.green : Color.yellow;
    }
}
