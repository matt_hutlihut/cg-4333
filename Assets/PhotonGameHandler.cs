﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class PhotonGameHandler : MonoBehaviour
{
    public int number = 10;
    public string roomObjectsResourcePath = "SphereColourChange";
    public string playerPrefabPath = "PlayerAvatar";

    void Start()
    {
        PhotonNetwork.IsMessageQueueRunning = true;
        if (PhotonNetwork.LocalPlayer.IsMasterClient)
        {
            for (int i = 0; i < number; i++)
            {
                Vector3 pos = new Vector3(Random.Range(-10f, 10f), 0f, Random.Range(-10f, 10f));
                PhotonNetwork.InstantiateRoomObject(roomObjectsResourcePath, pos, Quaternion.identity);
            }
        }
        
        // Now add our own players in
        Vector3 playerPos = new Vector3(Random.Range(-6f, 6f), 0f, Random.Range(-6f, 6f));
        PhotonNetwork.Instantiate(playerPrefabPath, playerPos, Quaternion.identity);
    }

    private void Update()
    {
        ScreenDebug.UpdateDebugLine("NetworkClientState:", PhotonNetwork.NetworkClientState);
        ScreenDebug.UpdateDebugLine("Room:", PhotonNetwork.NetworkClientState == ClientState.Joined ? PhotonNetwork.CurrentRoom.Name : "Not In Room");
    }

    public void OnQuitButtonPressed()
    {
        PhotonNetwork.AutomaticallySyncScene = false;
        PhotonNetwork.LeaveRoom(becomeInactive:false);
        SceneManager.LoadScene("CSLauncherScene");
    }
}
