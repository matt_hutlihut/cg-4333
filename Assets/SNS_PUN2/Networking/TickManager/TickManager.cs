﻿//Copyright 2018, Davin Carten, All rights reserved

using emotitron.Utilities.Networking;
using System.Collections.Generic;
using UnityEngine;

#if PUN_2_OR_NEWER
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
#endif

namespace emotitron.Networking.Internal
{
	
	public class TickManager
	{
		public readonly static Dictionary<int, ConnectionTick> perConnOffsets = new Dictionary<int, ConnectionTick>();
		public readonly static List<int> connections = new List<int>();

		public static TickManager single;

		/// <summary>
		/// Flag indicates that the next update should be flagged as needing to be reliable.
		/// </summary>
		public static bool needToSendInitialForNewConn;

		// Cached values
		//static int frameCount ;
		//static int halfFrameCount;

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		public static void Bootstrap()
		{
			single = new TickManager();
			MasterNetAdapter.onClientConnectCallback += OnClientConnect;
			MasterNetAdapter.onClientDisconnectCallback += OnClientDisconnect;
			//Photon.Pun.PhotonNetwork.NetworkingClient.AddCallbackTarget(single);
		}


		//#region PUN Room Callbacks

		//public void OnPlayerEnteredRoom(Player newPlayer)
		//{
		//	throw new System.NotImplementedException();
		//}

		//public void OnPlayerLeftRoom(Player otherPlayer)
		//{
		//	throw new System.NotImplementedException();
		//}

		//public void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged) { }
		//public void OnPlayerPropertiesUpdate(Player targetPlayer, Hashtable changedProps) { }
		//public void OnMasterClientSwitched(Player newMasterClient) { }

		//#endregion PUN Room Callbacks

		/// <summary>
		/// Run this prior to OnSnapshot, to establish if the number of snapshots for connection objects needs to be a value other than 1.
		/// </summary>
		public static void PreSnapshot()
		{
			for (int i = 0; i < connections.Count; ++i)
				if (!ReferenceEquals(perConnOffsets[connections[i]], null))
					perConnOffsets[connections[i]].SnapshotAdvance();
		}

		public static void PostSnapshot()
		{
			for (int i = 0; i < connections.Count; ++i)
				if (!ReferenceEquals(perConnOffsets[connections[i]], null))
					perConnOffsets[connections[i]].PostSnapshot();
		}


		/// <summary>
		/// Notify tick manager of an incoming frame, so it can register/modify offsets for that connection.
		/// </summary>
		/// <returns>FrameId translated into localFrameId</returns>
		public static int LogIncomingFrame(int connId, int originFrameId)
		{
			ConnectionTick offsets;

			int frameCount = NetMasterSettings.frameCount;

			if (!perConnOffsets.TryGetValue(connId, out offsets) || offsets == null)
			{
				LogNewConnection(connId, originFrameId, frameCount, out offsets);
			}

			/// In the future, we should be making use of localframe, for now it is the same as originframe for PUN
			int localFrameId = originFrameId += offsets.originToLocal;
			if (localFrameId >= frameCount)
				localFrameId -= frameCount;

			int currTargFrameId = NetMaster.CurrentFrameId; // offsets.currTargFrameId;

			bool frameIsInFuture;

			if (localFrameId == currTargFrameId)
			{
				frameIsInFuture = false;
			}
			else
			{
				/// Flag frame as valid if it is still in the future
				int frameOffsetFromCurrent = localFrameId - currTargFrameId;
				if (frameOffsetFromCurrent < 0)
					frameOffsetFromCurrent += frameCount;

				frameIsInFuture = frameOffsetFromCurrent !=0 && frameOffsetFromCurrent < NetMasterSettings.halfFrameCount;
			}

#if UNITY_EDITOR

			if (!frameIsInFuture)
			{
				int currSnapFrameId = NetMaster.PreviousFrameId;

				const string STR_TAG = "\nSeeing many of these indicates an unstable connection, too small a buffer setting, or too high a tick rate.";

				if (localFrameId == currTargFrameId)
				{
					if (NetMasterSettings.LogLevel == NetMasterSettings.LogInfoLevel.All)
					{
						string strframes = " Incoming Frame: " + localFrameId + " Current Interpolation: " + currSnapFrameId + "->" + currTargFrameId;
						Debug.Log("<b>Late Update </b>" + strframes + ". Already interpolating to this frame. Not critically late, but getting close." + STR_TAG);
					}
					
				}
				else if (localFrameId == currSnapFrameId)
				{
					if (NetMasterSettings.LogLevel >= NetMasterSettings.LogInfoLevel.WarningsAndErrors)
					{
						string strframes = " Incoming Frame: " + localFrameId + " Current Interpolation: " + currSnapFrameId + "->" + currTargFrameId;
						Debug.LogWarning("<b>Critically Late Update</b>" + strframes + " Already applied and now interpolating from this frame. Likely data loss." + STR_TAG);
					}
				}
				else
				{
					if (NetMasterSettings.LogLevel >= NetMasterSettings.LogInfoLevel.WarningsAndErrors)
					{
						string strframes = " Incoming Frame: " + localFrameId + " Current Interpolation: " + currSnapFrameId + "->" + currTargFrameId;
						Debug.LogWarning("<b>Critically Late Update</b> " + strframes + " Already applied this frame.  Likely data loss." + STR_TAG);
					}
				}
			}

#endif

			offsets.frameArrivedTooLate |= !frameIsInFuture;
			offsets.validFrames.Set(localFrameId, frameIsInFuture);

			return localFrameId;
		}

		private static void LogNewConnection(int connId, int originFrameId, int frameCount, out ConnectionTick offsets)
		{
			int currentFrame = NetMaster.CurrentFrameId;

			/// Apply default offset from current local frame
			int startingFrameId = currentFrame + (NetMasterSettings.targetBufferSize /*+ 1*/);
			while (startingFrameId >= frameCount)
				startingFrameId -= frameCount;

			int originToLocal = startingFrameId - originFrameId;
			if (originToLocal < 0)
				originToLocal += frameCount;

			int localToOrigin = frameCount - originToLocal;
			if (localToOrigin < 0)
				localToOrigin += frameCount;

			/// Curently local and origin are the same.
			/// TODO: Pool these
			offsets = new ConnectionTick(originToLocal, localToOrigin);

			AddConnection(connId, offsets);

		}

		private static void OnClientConnect(object connObj, int connId)
		{
			AddConnection(connId);
		}

		private static void OnClientDisconnect(object connObj, int connId)
		{
			RemoveConnection(connId);
		}

		private static void AddConnection(int connId, ConnectionTick offsets = null)
		{
#if PUN_2_OR_NEWER
            /// We don't treat own own connection as a thing
            if (PhotonNetwork.LocalPlayer.ActorNumber == connId)
				return;
#endif
			if (!connections.Contains(connId))
			{
				perConnOffsets.Add(connId, offsets);
				connections.Add(connId);
				/// Add this connection to the NetSends list of targets for a reliable update.
				NetMsgSends.reliableTargets.Add(connId);
				needToSendInitialForNewConn = true;
			}
			else
			{
				perConnOffsets[connId] = offsets;
			}
		}

		public static void RemoveConnection(int connId)
		{
			if (perConnOffsets.ContainsKey(connId))
			{
				perConnOffsets.Remove(connId);
				connections.Remove(connId);
			}
		}

	}
}

