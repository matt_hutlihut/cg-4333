﻿using UnityEngine;
using emotitron.Compression;
using emotitron.Networking;
using System.Collections.Generic;

#if PUN_2_OR_NEWER
using Photon.Pun;
#endif

namespace emotitron
{
	public enum TestEnum { None, Some, SomeMore, All }

	///// Structs only work if unsafe is enabled
	//[PackObject]
	//public struct TestStruct2
	//{

	//	[PackRangedInt(-20, 100, applyCallback = "HealthCallback", snapshotCallback = "HealthSnapshot")]
	//	public int myHealth;


	//	[PackRangedInt(0, 3)]
	//	public TestEnum myTestEnum;

	//	[Pack]
	//	public float floaterBloater;

	//	[Pack]
	//	public float floaterBloater2;

	//	public void HealthSnapshot(int snap, int targ)
	//	{

	//	}

	//	public void HealthCallback(int newvalue, int oldvalue)
	//	{
	//		// Put callback stuff here
	//	}
	//}


	[PackObject]
	public class TestPackObject : NetComponent
		, IOnPreSimulate
		, IOnPostSimulate
	{

		[PackHalfFloat(
			snapshotCallback = "SnapshotHook",
			applyCallback = "RotationHook", 
			setValueTiming = SetValueTiming.BeforeCallback, 
			interpolate = true, 
			keyRate = KeyRate.Every
			)]
		public float rotation;

		[Pack]
		public Vector3 v5;

		//[Pack]
		//public List<int> bytelist = new List<int>(3) { 11, 22, 33 };

		[PackRangedInt(-1, 2)]
		public int intoroboto;

		//[Pack]
		//public TestStruct2 teststruct;


		public void RotationHook(float newrot, float oldrot)
		{
			//Debug.Log("Hook  " + NetMaster.PreviousFrameId + ": " + oldrot + " ---  " + NetMaster.CurrentFrameId + ": " + newrot);

		}

		public void SnapshotHook(float snap, float targ)
		{
			//Debug.Log("Snap " +  NetMaster.PreviousFrameId+ ": "+ snap + " ---  " +  NetMaster.CurrentFrameId + ": " + targ);
		}

		public void OnPreSimulate(int frameId, int subFrameId)
		{
			if (IsMine)
			{
				rotation = (Mathf.Sin(Time.time) + .5f) * 120f; // (rotation + 5f);
				//int revs = (int)(rotation / 360);
				//rotation -= (revs * 360);
				transform.localEulerAngles = new Vector3(0, rotation, 0);
			}
		}

		public void OnPostSimulate(int frameId, int subFrameId, bool isNetTick)
		{
			if (!IsMine)
				transform.localEulerAngles = new Vector3(0, rotation, 0);
		}
		// Update is called once per frame
		void FixedUpdate()
		{
			
		}

		private void Update()
		{
			

		}
	}

}