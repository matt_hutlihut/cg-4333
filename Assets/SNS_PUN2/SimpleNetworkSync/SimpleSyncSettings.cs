﻿using UnityEngine;
using emotitron.Utilities.GUIUtilities;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace emotitron.Networking
{

#if UNITY_EDITOR
	[HelpURL(HELP_URL)]
#endif

	public class SimpleSyncSettings : SettingsScriptableObject<SimpleSyncSettings>
	{
		public const string MENU_PATH = "Window/PUN2 Simple Sync/";

#if UNITY_EDITOR
		public const string HELP_URL = "https://docs.google.com/document/d/1ySmkOBsL0qJnIk7iN9lbXPlfmYTGkN7JFgKDBdqj9e8/edit#bookmark=id.fd0tg2ybh584";
		public override string HelpURL { get { return HELP_URL; } }
		public static string instructions = "";

		[MenuItem(MENU_PATH + "Simple Sync Settings", false, 1)]
		private static void PingSettings()
		{
			Selection.activeObject = Single;
			EditorGUIUtility.PingObject(single);
		}

		[UnityEditor.InitializeOnLoadMethod]
		public static void InitializeOnLoad()
		{
#if !PUN_2_OR_NEWER
			Debug.LogWarning("<b>PhotonEngine PUN2 NOT INSTALLED!</b>");
#endif
		}

#endif

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		public static void Bootstrap()
		{
			var single = Single;
		}

#if UNITY_EDITOR

		public static void DrawGuiStatic(Object target, bool asFoldout, bool includeScriptField, bool initializeAsOpen = true, bool asWindow = false)
		{
			Single.DrawGui(target, asFoldout, includeScriptField, initializeAsOpen, asWindow);
		}
		public override bool DrawGui(Object target, bool asFoldout, bool includeScriptField, bool initializeAsOpen = true, bool asWindow = false)
		{
			
			NetMasterSettings.Single.DrawGui(target, true, false, true);

			Compression.PackObjectSettings.Single.DrawGui(target, true, false, true);

			return true;
		}

#endif

	}

#if UNITY_EDITOR

	[CustomEditor(typeof(SimpleSyncSettings))]
	[CanEditMultipleObjects]
	public class SimpleSyncSettingsEditor : Editor
	{

		public override void OnInspectorGUI()
		{
			SimpleSyncSettings.Single.DrawGui(target, false, false, true);
		}
	}
#endif
}
