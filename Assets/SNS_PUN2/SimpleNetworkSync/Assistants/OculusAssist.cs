﻿#if UNITY_EDITOR

using emotitron.Utilities;
using emotitron.Utilities.Networking;
using System;
using UnityEditor;
using UnityEngine;

namespace emotitron.Networking.Assists
{
	public static class OculusAssist
	{



		[MenuItem(AssistHelpers.CONVERT_TO_FOLDER + "Oculus", false, -999)]
		public static void ConvertOculus()
		{
			var selection = NetObjectAssists.ConvertToBasicNetObject(null);

			if (selection == null)
				return;

			var t = selection.transform;

			/// Add root transform sync
			if (HasOculusController(t))
				selection.transform.Add3dPosOnly().transform.Add3dEulerOnly();

			selection.EnsureComponentExists<AutoDisableOculusObjects>();

			/// Tracking Space
			var trackingSpace = t.RecursiveFind("TrackingSpace");
			trackingSpace.Add3dEulerOnly();

			/// Hands
			var leftHandAnchor = t.RecursiveFind("LeftHandAnchor");
			var rghtHandAnchor = t.RecursiveFind("RightHandAnchor");
			leftHandAnchor.Add3DHandsPos().transform.Add3DHandsRot();
			rghtHandAnchor.Add3DHandsPos().transform.Add3DHandsRot();

			/// OVRCameraRig
			var cameraRig = t.RecursiveFind("OVRCameraRig");
			if (cameraRig)
				cameraRig.gameObject.EnsureComponentExists<AutoOwnerComponentEnable>();

			/// Add all SyncAnimator
			t.EnsureComponentExists<SyncAnimator, Animator>(true);

			selection.EnsureComponentExists<AutoDestroyUnspawned>();

			/// Move OVRManager off of this object and onto a non-net object in the scene.
			Type ovrManagerType = Type.GetType("OVRManager");
			if (ovrManagerType == null)
				ovrManagerType = Type.GetType("OVRManager, Oculus.VR, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");

			if (ovrManagerType != null)
			{
				var manager = t.GetComponentInChildren(ovrManagerType);
				if (manager)
				{
					manager.ComponentCopy(new GameObject("OVRManager"));
					GameObject.DestroyImmediate(manager);
				}
			}
		}

		private static bool HasOculusController(Transform t)
		{

			/// TODO: add other known root movement components for Oculus here as I find them
			if (t.HasComponent("SimpleCapsuleWithStickMovement", "SimpleCapsuleWithStickMovement, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null"))
				return true;

			return false;
		}


	}
}

#endif

