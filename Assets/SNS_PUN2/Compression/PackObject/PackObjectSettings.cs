﻿using emotitron.Utilities.GUIUtilities;
using UnityEngine;


#if UNITY_EDITOR
using UnityEditor;
#endif

namespace emotitron.Compression
{
#if UNITY_EDITOR
    [HelpURL(HELP_URL)]
#endif

    public class PackObjectSettings : SettingsScriptableObject<PackObjectSettings>
    {

#if UNITY_EDITOR

        public const string HELP_URL = "";
        public override string HelpURL { get { return HELP_URL; } }
#endif

        [Header("Code Generation")]
        [Tooltip("Enables the codegen for PackObjects / PackAttributes. Disable this if you would like to suspend codegen. Existing codegen will remain, unless it produces errors.")]
        public bool enableCodegen = true;

        [Tooltip("Automatically deletes codegen if it produces any compile errors. Typically you will want to leave this enabled.")]
        public bool deleteBadCode = true;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Bootstrap()
        {
            var single = Single;
        }

#if UNITY_EDITOR

		public override bool DrawGui(Object target, bool asFoldout, bool includeScriptField, bool initializeAsOpen = true, bool asWindow = false)
		{
			bool expanded = base.DrawGui(target, asFoldout, includeScriptField, initializeAsOpen, asWindow);

			if (GUI.Button(EditorGUILayout.GetControlRect(), "Regenerate All Code"))
			{
				Compression.Internal.TypeCatalogue.RebuildSNSCodegen();
			}

			return expanded;
		}

#endif

	}

#if UNITY_EDITOR

    [CustomEditor(typeof(PackObjectSettings))]
    [CanEditMultipleObjects]
    public class PackObjectSettingsEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            PackObjectSettings.Single.DrawGui(target, false, false, true);
        }
    }
#endif

}
